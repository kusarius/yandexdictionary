﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Json;
using System.Net;
using System.Threading.Tasks;

namespace YandexDictionary
{
    // Фрагмент, показывающй панель перевода
    internal class DictionaryTabFragment : Fragment
    {
        Spinner fromLang, toLang; 

        private Activity context;
        private const string apiKey = "dict.1.1.20170621T132508Z.fa1d84bbf5160a8d.782f7b67550edddcf6acb7d1a0b0fff669bde502";

        // Используется для обрбот нажатия пункта в истории
        public static bool isFromHistory = false;
        public static string historyEntry;

        public DictionaryTabFragment(Activity a)
        {
            context = a;
        }

        public override View OnCreateView(LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            View view = inflater.Inflate(Resource.Layout.DictionaryTab, container, false);

            fromLang = view.FindViewById<Spinner>(Resource.Id.spinner1);
            toLang = view.FindViewById<Spinner>(Resource.Id.spinner2);

            // Заполняем списки языков
            string[] langs = { "Английский", "Русский", "Украинский", "Немецкий" };
            ArrayAdapter<string> adapter = new ArrayAdapter<string>(context, Android.Resource.Layout.SimpleSpinnerDropDownItem, langs);
            fromLang.Adapter = adapter;
            toLang.Adapter = adapter;

            toLang.SetSelection(1);
            
            // Устанавливаем действие для нажатия на кнопку со стрелками
            ImageView reverseLangButton = view.FindViewById<ImageView>(Resource.Id.imageView1);
            reverseLangButton.Click += delegate
            {
                // Меняем местами языки
                int temp = fromLang.SelectedItemPosition;
                fromLang.SetSelection(toLang.SelectedItemPosition);
                toLang.SetSelection(temp);
            };

            // Устанавливаем действие для нажатия на кнопку "Перевести"
            Button translateButton = view.FindViewById<Button>(Resource.Id.button1);
            translateButton.Click += delegate { OnTranslateButtonClick(view); };

            TextView tv = view.FindViewById<TextView>(Resource.Id.editText1);

            // Если мы нажали на пункт истории, то восстанавливаем текст в поле для ввода
            if (isFromHistory)
            {
                tv.Text = historyEntry;
                isFromHistory = false;
            }

            return view;
        }

        private string getLangCode(string lang)
        {
            switch (lang)
            {
                case "Русский": return "ru";
                case "Украинский": return "uk";
                case "Английский": return "en";
                case "Немецкий": return "de";
                default: return "";
            }
        }

        private async void OnTranslateButtonClick(View v)
        {
            // Очищаем все поля
             v.FindViewById<TextView>(Resource.Id.textView1).Text = "";
            v.FindViewById<TextView>(Resource.Id.textView2).Text = "";
            v.FindViewById<ListView>(Resource.Id.listView1).Adapter = null;

            // Составляем запрос к API
            TextView inputText = v.FindViewById<TextView>(Resource.Id.editText1);
            string trdir = getLangCode((string)fromLang.SelectedItem) + "-" + getLangCode((string)toLang.SelectedItem);
            string query = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?ui=ru&key=" + apiKey +
                "&lang=" + trdir + "&text=" + inputText.Text;

            // Выполняем запрос и парсим результат
            string json = await new WebClient().DownloadStringTaskAsync(query);
            RootObject res = JsonConvert.DeserializeObject<RootObject>(json);
            TextView translationLabel = v.FindViewById<TextView>(Resource.Id.textView1);

            // Если мы не получили ни одного перевода
            if (res.def.Count == 0)
            {
                // Тогда просто выведем в текстовое поле то, что ввёл пользователь
                translationLabel.Text = inputText.Text;
                return;
            }

            // "Главный" перевод
            translationLabel.Text = res.def[0].tr[0].text;

            // Транскрипция
            TextView transcriptionLabel = v.FindViewById<TextView>(Resource.Id.textView2);
            transcriptionLabel.Text = res.def[0].text + " [" + res.def[0].ts + "]";
            
            // Получаем список всех статей в словаре и устанавливаем их номера
            List<Tr> trList = new List<Tr>();
            foreach (Def d in res.def)
            {
                int num = 1;
                foreach (Tr t in d.tr)
                {
                    t.num = num;
                    num++;

                    trList.Add(t);
                }
            }

            // Заполняем список переведённым текстом
            TranslationAdapter adapter = new TranslationAdapter(context, trList);
            v.FindViewById<ListView>(Resource.Id.listView1).Adapter = adapter;

            // Если перевод удался - записываем об этом в историю
            HistoryTabFragment.history.Add(new Translation(res.def[0].tr[0].text, res.def[0].text));

        }
    }
}