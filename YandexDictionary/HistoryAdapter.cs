﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace YandexDictionary
{
    // Адаптер для списка, хранящий историю переводов
    class HistoryAdapter : BaseAdapter<Translation>
    {
        List<Translation> items;
        Activity context;

        public HistoryAdapter(Activity context, List<Translation> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Translation this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public void updateList(List<Translation> newlist)
        {
            items.Clear();
            items.AddRange(newlist);
            this.NotifyDataSetChanged();
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem2, null);
            view.FindViewById<TextView>(Android.Resource.Id.Text2).Text = items[position].input;
            view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = items[position].translation;
            return view;
        }
    }
}