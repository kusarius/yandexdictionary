﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics.Drawables;

namespace YandexDictionary
{
    [Activity(Label = "Словарь", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Отключаем показ заголовка окна
            ActionBar.SetDisplayShowTitleEnabled(false);
            ActionBar.SetDisplayShowHomeEnabled(false);

            // Включаем режим показа вкладок
            ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;

            // Создаём вкладки и связываем соответствующие фрагменты с ними
            var dictionaryTab = ActionBar.NewTab();
            dictionaryTab.SetIcon(Resource.Drawable.dic);
            
            dictionaryTab.TabSelected += delegate (object sender, ActionBar.TabEventArgs e) {
                e.FragmentTransaction.Replace(Resource.Id.fragmentContainer, new DictionaryTabFragment(this));
            };

            var historyTab = ActionBar.NewTab();
            historyTab.SetIcon(Resource.Drawable.bookmark);

            historyTab.TabSelected += delegate (object sender, ActionBar.TabEventArgs e) {
                e.FragmentTransaction.Replace(Resource.Id.fragmentContainer, new HistoryTabFragment(this));
            };

            var creditsTab = ActionBar.NewTab();
            creditsTab.SetIcon(Resource.Drawable.settings);

            creditsTab.TabSelected += delegate (object sender, ActionBar.TabEventArgs e) {
                e.FragmentTransaction.Replace(Resource.Id.fragmentContainer, new CreditsTabFragment());
            };

            // Добавляем вкладки на окно
            ActionBar.AddTab(dictionaryTab);
            ActionBar.AddTab(historyTab);
            ActionBar.AddTab(creditsTab);

            ActionBar.SetStackedBackgroundDrawable(new ColorDrawable(Android.Graphics.Color.Rgb(225, 194, 85)));
            ActionBar.SetSplitBackgroundDrawable(new ColorDrawable(Android.Graphics.Color.Black));
        }
    }
}

