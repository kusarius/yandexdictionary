﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace YandexDictionary
{
    // Представляет событие перевода - входное слово и его перевод
    class Translation
    {
        public Translation(string i, string t)
        {
            input = i;
            translation = t;
        }

        public string input { get; set; }
        public string translation { get; set; }
    }
}