﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using System;

namespace YandexDictionary
{
    // Фрагмент, хранящий список всех переводов в текущей сессии
    internal class HistoryTabFragment : Fragment
    {
        public static List<Translation> history = new List<Translation>();
        Activity context;

        public HistoryTabFragment(Activity a)
        {
            context = a;
        }

        public override View OnCreateView(LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            View view = inflater.Inflate(Resource.Layout.HistoryTab, container, false);
            view.FindViewById<ListView>(Resource.Id.historyListView).Adapter = new HistoryAdapter(context, history);

            // Устанавливаем действие для кнопки "Очистить историю"
            view.FindViewById<Button>(Resource.Id.clearHistoryButton).Click += delegate
            {
                history.Clear();

                // Обновляем список после очистки
                ((HistoryAdapter)view.FindViewById<ListView>(Resource.Id.historyListView).Adapter).updateList(history);
            };

            // Действие для нажатия на пункт списка
            view.FindViewById<ListView>(Resource.Id.historyListView).ItemClick += listItemClick;

            return view;
        }

        private void listItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            // Переходим на вкладку с словарем и устанавливаем значение в поле для ввода 

            string text = history[e.Position].translation;
            DictionaryTabFragment.isFromHistory = true;
            DictionaryTabFragment.historyEntry = text;
            context.ActionBar.SetSelectedNavigationItem(0);
        }
    }
}