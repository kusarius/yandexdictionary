﻿using Android.App;
using Android.OS;
using Android.Views;

namespace YandexDictionary
{
    // Фрагмент, показывающий информацию об авторе
    internal class CreditsTabFragment : Fragment
    {
        public override View OnCreateView(LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            return inflater.Inflate(Resource.Layout.CreditsTab, container, false);
        }
    }
}