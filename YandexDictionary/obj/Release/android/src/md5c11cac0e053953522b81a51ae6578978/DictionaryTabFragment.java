package md5c11cac0e053953522b81a51ae6578978;


public class DictionaryTabFragment
	extends android.app.Fragment
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreateView:(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;:GetOnCreateView_Landroid_view_LayoutInflater_Landroid_view_ViewGroup_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("YandexDictionary.DictionaryTabFragment, YandexDictionary, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", DictionaryTabFragment.class, __md_methods);
	}


	public DictionaryTabFragment () throws java.lang.Throwable
	{
		super ();
		if (getClass () == DictionaryTabFragment.class)
			mono.android.TypeManager.Activate ("YandexDictionary.DictionaryTabFragment, YandexDictionary, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public DictionaryTabFragment (android.app.Activity p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == DictionaryTabFragment.class)
			mono.android.TypeManager.Activate ("YandexDictionary.DictionaryTabFragment, YandexDictionary, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.App.Activity, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public android.view.View onCreateView (android.view.LayoutInflater p0, android.view.ViewGroup p1, android.os.Bundle p2)
	{
		return n_onCreateView (p0, p1, p2);
	}

	private native android.view.View n_onCreateView (android.view.LayoutInflater p0, android.view.ViewGroup p1, android.os.Bundle p2);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
