﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace YandexDictionary
{
    // Адаптер для списка, в котором отображаются словарные статьи 
    class TranslationAdapter : BaseAdapter<Tr>
    {
        Activity context;
        List<Tr> items;

        public TranslationAdapter(Activity context, List<Tr> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Tr this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            // Формируем все элементы списка...

            var item = items[position];
            View view = context.LayoutInflater.Inflate(Resource.Layout.TranslationListItem, null);

            // Устанавливаем часть речи и номер статьи 
            view.FindViewById<TextView>(Resource.Id.posTextView).Text = item.pos;
            view.FindViewById<TextView>(Resource.Id.numTextView).Text = item.num.ToString();

            // Формируем и устанавливаем все синонимы
            string syns = item.text + (!string.IsNullOrEmpty(item.gen) ? " (" + item.gen + ")" : "");
            if (item.syn != null)
            {
                foreach (Syn s in item.syn)
                    syns += ", " + s.text + (!string.IsNullOrEmpty(s.gen) ? " (" + s.gen + ")" : "");
            }

            view.FindViewById<TextView>(Resource.Id.synTextView).Text = syns; 

            // Формируем все связанные понятия (смыслы)
            // Если их нет, тогда не отображаем поле
            if (item.mean != null)
            {
                string means = "(";
                foreach (Mean m in item.mean)
                    means += m.text + ", ";
                means = means.Substring(0, means.Length - 2) + ")";

                view.FindViewById<TextView>(Resource.Id.meanTextView).Text = means;
            }
            else view.FindViewById<TextView>(Resource.Id.meanTextView).Visibility = ViewStates.Gone;

            // Формируем список примеров
            // Если их нет, не отображаем поле
            if (item.ex != null)
            {
                string exs = "";
                foreach (Ex e in item.ex)
                    exs += e.text + " - " + e.tr[0].text + System.Environment.NewLine;

                view.FindViewById<TextView>(Resource.Id.exTextView1).Text = exs.Substring(0, exs.Length - 1);
            }
            else view.FindViewById<TextView>(Resource.Id.exTextView1).Visibility = ViewStates.Gone;

            return view;
        }
    }
}